﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ScrollingBackground : MonoBehaviour {

	private float tileWidth;

	private Vector2 newPosition;
	public float scrollSpeed = 0f;
	
	private float camLeft;

	private Car car;

	public bool spawnSheep = false;
	public GameObject sheep;
	public GameObject sheepInstance;

	public float middleLane;
	public float leftLane;
	public float rightLane;

	// Use this for initialization
	void Start () {

		middleLane = -2.45f;
		leftLane = middleLane + 0.8f;
		rightLane = middleLane - 0.8f;

		car = GameObject.Find ("bluecarbody").GetComponent<Car>();

		// work out the width of the background tile
		SpriteRenderer renderer = GetComponent<SpriteRenderer> ();
		tileWidth = renderer.bounds.size.x;

		// take a reference to transform.position for convenience
		newPosition = transform.position;

		camLeft = 0 - tileWidth / 2;
	}

	void SpawnSheep() {
		Debug.Log ("New sheep! Baaaa!");
		sheepInstance = Instantiate (sheep);

		float sheepPosition = -0.8f;

		Vector3 newScale = sheepInstance.transform.localScale;
		int lane = UnityEngine.Random.Range (0, 4);
		if (lane == 1) {
			sheepPosition = leftLane;
			newScale.x *= 1.0f;
			newScale.y *= 1.0f;
		} else if (lane == 2) {
			sheepPosition = middleLane;
			newScale.x *= 1.2f;
			newScale.y *= 1.2f;
		} else if (lane == 3) {
			sheepPosition = rightLane;
			newScale.x *= 1.4f;
			newScale.y *= 1.4f;
		}


		sheepInstance.transform.parent = gameObject.transform;
		sheepInstance.transform.localPosition = new Vector2(0, sheepPosition);
		sheepInstance.GetComponent<SpriteRenderer> ().sortingOrder = 1;


		sheepInstance.transform.localScale = newScale;

		Debug.Log ("Sheep in lane " + lane);

		if (lane > car.lane + 1) {
			sheepInstance.GetComponent<SpriteRenderer> ().sortingOrder = car.GetComponent<SpriteRenderer>().sortingOrder + 2;	
		}

		sheepInstance.GetComponent<Sheep> ().lane = lane;
	}

	// Update is called once per frame
	void Update () {

		if (Input.GetKeyUp (KeyCode.S)){
			SpawnSheep ();
		}

		// if tile has scrolled out of the screen, shift it
		// to the right ready to scroll past again
		if (transform.position.x + (tileWidth / 2) < camLeft) {
			newPosition.x += (2f * tileWidth);
			transform.position = newPosition;

			// Occasionally spawn a sheep for company
			if (spawnSheep && UnityEngine.Random.Range (0,10) > 5)
			{
				SpawnSheep ();
			}
		}

		// accelerate the background up to the current speed
		newPosition.x -= Time.deltaTime * scrollSpeed;
		transform.position = newPosition;


	}
}
