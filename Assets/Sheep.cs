﻿using UnityEngine;
using System.Collections;

public class Sheep : MonoBehaviour {

	AudioSource audiosrc;
	public AudioSource baa;
	GameObject car;
	bool hasBleated = false;
	public Sprite splatSprite;
	public int lane;
	bool dead = false;
	AudioSource[] audioSources;

	// Use this for initialization
	void Start () {
		audioSources = GetComponents<AudioSource> ();
		audiosrc = audioSources [0];
		car = GameObject.Find ("bluecarbody");
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.GetKeyUp (KeyCode.S))
			audiosrc.PlayOneShot (audiosrc.clip);
	
		if (!dead && !hasBleated && transform.position.x < car.transform.position.x + 1f) {
			audiosrc.PlayOneShot (audiosrc.clip);
			hasBleated = true;
		}


		if (lane == car.GetComponent<Car> ().lane + 1) {
			if (transform.position.x <= car.transform.position.x + (car.GetComponent<SpriteRenderer>().bounds.size.x / 2))
			{
				Splat ();
			}
		}

	}

	void Splat() {
		if (!dead) {
			SpriteRenderer sr = GetComponent<SpriteRenderer> ();
			sr.sprite = splatSprite;
			transform.position = new Vector2(transform.position.x, transform.position.y - 1f);
			audioSources[1].PlayOneShot (audioSources[1].clip);
			Debug.Log ("Sheep splatted");
			dead = true;
		}
	}

	void OnBecameInvisible() {
		Debug.Log ("The sheep is gone!");
		GetComponent<SpriteRenderer> ().enabled = false;
		Destroy (gameObject, 2f);
	}


}
