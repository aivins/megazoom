﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;


public class Car : MonoBehaviour {

	GameObject carwheel1;
	GameObject carwheel2;
	
	float Speed  = 0f;
	public float MaxSpeed  = 100;
	public float Acceleration;
	public float Deceleration;

	public SpriteRenderer sr;

	Vector3 newScale;
	Vector3 newPosition;

	float wheelCircumference;

	public int lane = 1;
	
	// Use this for initialization
	void Start () {

		sr = GetComponent<SpriteRenderer> ();

		carwheel1 = GameObject.Find ("carwheel1");
		carwheel2 = GameObject.Find ("carwheel2");

		carwheel1.GetComponent<Animator> ().speed = 0;
		carwheel2.GetComponent<Animator> ().speed = 0;

		carwheel1.GetComponent<Animator>().enabled = true;
		carwheel2.GetComponent<Animator>().enabled = true;

		wheelCircumference = carwheel1.GetComponent<SpriteRenderer>().bounds.size.x * (float)Math.PI;

		newPosition = transform.position;
		newScale = transform.localScale;

	}

	float getKmh() {
		return wheelCircumference * Speed * 60 * 60 / 1000;
	}

	void setKmh(float target) {
		Speed = (target / wheelCircumference / 60 / 60) * 1000;
	}

	void laneLeft() {
		if (lane > 0 && Speed > 0) {
			newScale = transform.localScale;
			newPosition = transform.position;
			newScale.x -= 0.15f;
			newScale.y -= 0.15f;
			newPosition.y += 1.6f;
			lane--;
			Debug.Log ("Left a lane " + newPosition.y);
		}
	}

	void laneRight() {
		if (lane < 2 && Speed > 0) {
			newScale = transform.localScale;
			newPosition = transform.position;
			newScale.x += 0.15f;
			newScale.y += 0.15f;
			newPosition.y -= 1.6f;
			lane++;
			Debug.Log ("Right a lane " + newPosition.y);
		}
	}

	void Update () {

		if (Input.GetKeyUp (KeyCode.LeftArrow)) {
			laneLeft ();
		}
		else if (Input.GetKeyUp (KeyCode.RightArrow)) {
			laneRight();
		}

		if ((Input.GetKey (KeyCode.UpArrow)) && (getKmh () < MaxSpeed)) {
			Speed = Speed + Acceleration * Time.deltaTime;
			if (getKmh () > MaxSpeed)
				setKmh (MaxSpeed);
		}
		else {

			if (Input.GetKey (KeyCode.DownArrow)) {
		
				if (Speed > Deceleration * Time.deltaTime) {
					Speed = Speed - Deceleration * Time.deltaTime;
				} else if (Speed < -Deceleration * Time.deltaTime) {
					Speed = Speed + Deceleration * Time.deltaTime;
				} else { 
					Speed = 0;
				}
			}

		}

		if (transform.position != newPosition) {
			transform.position = Vector3.Lerp (transform.position, newPosition, Speed/25);
		}

		if (transform.localScale != newScale) {
			transform.localScale = Vector3.Lerp (transform.localScale, newScale, Speed/25);
		}


		ScrollingBackground bg1 = GameObject.Find ("background").GetComponent<ScrollingBackground> ();
		ScrollingBackground bg2 = GameObject.Find ("background2").GetComponent<ScrollingBackground> ();

		carwheel1.GetComponent<Animator> ().speed = Speed;
		carwheel2.GetComponent<Animator> ().speed = Speed;

		bg1.scrollSpeed = Speed * 5.4f;
		bg2.scrollSpeed = Speed * 5.4f;

		GetComponent<AudioSource> ().pitch = 1f + (getKmh () / 50f);

		GameObject.Find ("Speedo").GetComponent<Text> ().text = String.Format ("Speed: {0:0.00}km/h", getKmh ());
	}
}
